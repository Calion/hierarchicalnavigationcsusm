﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace navigationPage
{
    public partial class MyPageNext : ContentPage
    {
        public MyPageNext(string data)
        {
            InitializeComponent();

            Debug.WriteLine($"Debug {data}");
            label.Text = data;
        }
    }
}
