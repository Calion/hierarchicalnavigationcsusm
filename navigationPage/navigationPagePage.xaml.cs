﻿using Xamarin.Forms;

namespace navigationPage
{
    public partial class navigationPagePage : ContentPage
    {
        public navigationPagePage()
        {
            InitializeComponent();
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            if (editor.Text == null || editor.Text.Equals("")) {
                await DisplayAlert("Alert", "The field cannot be empty.", "Ok");
            } else {
                bool response = await DisplayAlert("Validation", $"Do you want to go to the next step with this value \"{editor.Text}\" ?", "Yes", "No");
                if (response)
                    await Navigation.PushAsync(new MyPageNext(editor.Text));   
            }
        }
    }
}
